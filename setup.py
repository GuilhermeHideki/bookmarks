from setuptools import setup

setup(
    name='bookmarks',
    version='0.0.1',
    packages=[''],
    url='',
    license='MIT',
    install_requires=[
        'blitzdb',
        'click',
        'requests'
    ],
    author='Guilherme Hideki Danno',
    author_email='Guilherme.danno@gmail.com',
    description='',
    entry_points={
        'console_scripts': [
            'bookmarks=bookmarks.cli.commands:main'
        ]
    }
)
