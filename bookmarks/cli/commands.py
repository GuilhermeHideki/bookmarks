#!python2
from __future__ import print_function, absolute_import, unicode_literals
from os import path, getenv
from re import match
import click
from requests import get
from bs4 import BeautifulSoup
from blitzdb import FileBackend
from bookmarks.models import Bookmark, Tag

click.disable_unicode_literals_warning = True

# TODO do something if not set on windows
backend = FileBackend(path.join(getenv("home"), '.bookmarks', 'db'))

# Config
validation_url_default = False
verbosity_level_default = 0

help = {}
help["bookmark"] = "URL of the bookmark"
help["tags"] = "Tag of the URL. Can be used multiple times"
help["verbose"] = "Verbosity level (default: {})" \
    .format(verbosity_level_default)
help["validation-url"] = "Validates the URL. (default: {})" \
    .format(validation_url_default)


@click.group()
@click.pass_context
def main(ctx):
    pass


@click.command(help="Add a bookmark")
@click.pass_context
@click.option('--url', '-u', help=help["bookmark"])
@click.option('--tags', '-t', help=help["tags"], multiple=True)
@click.option('-v', '--verbose', count=True, help=help["verbose"],
              default=verbosity_level_default)
@click.option('--validate/--no-validate', help=help["validation-url"],
              default=validation_url_default)
def add(ctx, url, tags, verbose, validate):
    click.echo()

    # TODO better user-agent header
    # TODO app name
    # TODO version
    headers = {'user-agent': '{}/{}'.format("my-bookmark-cli-app", "0.0.0")}
    r = get(url, headers=headers)

    # TODO make something better for other status codes
    if (not validate) or (validate and r.status_code == 200):
        if verbose:
            display_url(url)
            tags = list(set(tags))
            display_tags(tags)
            click.echo()

        backend.begin()
        ts = []
        for tag in tags:
            ts += [Tag.find_or_create_new(backend, tag)]

        page_title = BeautifulSoup(r.text, "html.parser").title.text
        b = Bookmark({
            'url': url,
            'tags': ts,
            'title': page_title
        })
        b.save(backend)

        if verbose >= 2:
            click.echo()
            click.echo(
                click.style("URL: ") +
                click.style(b.url, fg='green')
            )
            click.echo("Tags: {}".format(b.tags))
            click.echo("Title: {}".format(b.title))
            for tag in b.tags:
                click.echo("name: {} value: {}".format(
                    tag.name,
                    tag.value
                ))

        if (True):
            backend.commit()
    else:
        click.echo(r.status_code)


def display_url(url):
    m = match(r'(?P<p>https?)(://)(?P<w>w{3}\.)?(?P<u>\w+)'
              r'(?P<e>\.\w+)(?P<x>.+)?', url)
    addText = "Adding the url: "

    if (m):
        # TODO: Better color scheme
        try:
            color = 'white' if m.group('p') == "https" else 'yellow'
            click.echo(
                click.style(addText) +
                click.style(m.group('p'), fg=color, bold=True) +
                click.style(m.group(2)) +
                click.style(m.group('w'), fg='white') +
                click.style(m.group('u'), fg='white', bold=True) +
                click.style(m.group('e'), fg='white') +
                click.style(m.group('x'), fg='yellow')
            )
        except TypeError:
            click.echo(
                click.style(addText, fg='white', bold=True) +
                click.style(url, fg='green')
            )
            pass
    else:
        click.echo(
            click.style(addText, fg='white', bold=True) +
            click.style(url, fg='green')
        )


def display_tags(tags):
    click.secho("\nAdding the tags:", fg='white')
    for index, tag in enumerate(tags, start=1):
        click.echo(
            click.style("{:>4}: ".format(index)) +
            click.style(tag, fg='green'))


def list_of_bookmarks(b):
    """Returns a set of bookmarks in 'x' or 'x:y' format"""
    return set(
        ["{0.name}:{0.value}".format(t) for t in b.tags if t.value is not None]
        +
        [t.name for t in b.tags if t.value is None]
    )


@click.command(name="ls", help="Lists the bookmarks")
@click.pass_context
@click.option('-v', '--verbose', count=True, help=help["verbose"], default=0)
@click.option('-i', '--id', help="Id of the bookmark")
@click.option('--tags','-t', help=help["tags"], multiple=True)
# TODO pass parameters
def ls_bookmarks(ctx, verbose, id, tags):
    # Empty query = fetch all
    query = {}
    if id:
        query = {'pk': id}
    elif tags:
        query = {'tags': { Tag() }}
    bookmarks = backend.filter(Bookmark, query)

    click.echo()

    for index, b in enumerate(bookmarks, start=1):
        # Gets the tags x:y and x:None
        x = list_of_bookmarks(b)
        click.echo(
            click.style("{:>3}. ".format(index)) +
            click.style(b.url, fg='cyan')
        )
        try:
            click.echo(
                click.style("{:>5}{} ".format("", b.title))
            )
        except:
            pass

        click.echo(
            click.style("{:>5}[TAGS] ".format(""), fg='red') +
            click.style(', '.join(x), fg='yellow')
        )
        if verbose >= 2:
            click.echo(
                click.style("{:>5}id: ".format("")) +
                click.style(b.pk, fg='magenta')
            )

        click.echo()
    pass


rm = {}
rm['confirm_default'] = False
rm['help'] = {}
rm['help']['config'] = "Confirm deletion (default: {})".format(rm['confirm_default'])


@click.command(help="Remove a bookmark, by id")
@click.pass_context
@click.option('-v', '--verbose', count=True, help=help["verbose"], default=0)
@click.option('-i', '--id', help="Id of the bookmark")
@click.option('--yes/--no', help=rm['help']['config'], default=rm['confirm_default'])
def rm(ctx, verbose, id, yes):
    backend.delete(backend.get(Bookmark, {"pk": id}))
    # TODO if(verbose) print(bookmark)
    # TODO yes or prompt
    if yes:
        backend.commit()


main.add_command(add)
main.add_command(ls_bookmarks)
main.add_command(rm)
