#!python2
from __future__ import print_function, absolute_import
from re import split
from blitzdb import Document


class Bookmark(Document):
    pass


class Tag(Document):
    # TODO Extract to another class
    @staticmethod
    def find_or_create_new(backend, tag_str):
        # Parse input
        x = split(r'[:]', tag_str)
        t = Tag({
            'name': x[0],
            'value': None if len(x) < 2 else x[1]
        })
        try:
            return Tag.find(backend, t.name, t.value)
        # TODO Should try to catch the multiple entries exception an do something
        except:
            return t

    # TODO Extract to another class
    @staticmethod
    def find(backend, name, value):
        # return backend.get(Tag,{'name': name, 'value': value})
        return Tag.find(backend,{'name': name, 'value': value})

    @staticmethod
    def find(backend, **kwargs):
        return backend.get(Tag, kwargs)
